﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Clientes
{
    public class Direccion : Entity
    {
        public string Domicilio { get; set; }

        public string Localidad { get; set; }

        public string Provincia { get; set; }

        public string Pais { get; set; }


        public Direccion(string domicilio, string localidad, string provincia, string pais)
        {
            this.Domicilio = domicilio;
            this.Localidad = localidad;
            this.Provincia = provincia;
            this.Pais = pais;
        }

        public string toString()
        {
            return this.Domicilio + ", " + this.Localidad + ", " + this.Provincia + ", " + this.Pais; 
        }
    }
}
